// Import the discord.js module
const Discord = require('discord.js');
const music = require('discord.js-musicbot-addon');

// Create an instance of a Discord client
const client = new Discord.Client();
const prefix = ';;'; //You could also load this from a .json file as well.

// The token of your bot - https://discordapp.com/developers/applications/me
const token = 'MzYzMTIyMTU0Mjg1NDMyODUy.DK_WEA.teXPo3ff7ZNNOrK2RakkTFqYWzw';

// Bot logs in
client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

// Bot sends back date and time it connects
client.on('ready', () => {
    console.log(`[Start] ${new Date()}`);
});

// Basic pong message
client.on('message', msg => {
  if (msg.content === 'ping') {
    msg.reply('Pong!');
  }
});

// Bot joins the channel where user is that executed the /join command
client.on('message', message => {
    // Voice only works in guilds, if the message does not come from a guild,
    // we ignore it
    if (!message.guild) return;
  
    if (message.content === '/join') {
      // Only try to join the sender's voice channel if they are in one themselves
      if (message.member.voiceChannel) {
        message.member.voiceChannel.join()
          .then(connection => { // Connection is an instance of VoiceConnection
            message.reply('I have successfully connected to the channel!');
          })
          .catch(console.log);
      } else {
        message.reply('You need to join a voice channel first!');
      }
    }
  });
  
// Create an event listener for new guild members
client.on('guildMemberAdd', member => {
    // Send the message to a designated channel on a server:
    const channel = member.guild.channels.find('name', 'member-log');
    // Do nothing if the channel wasn't found on this server
    if (!channel) return;
    // Send the message, mentioning the member
    channel.send(`Welcome to the server, ${member}`);
  });

// Bot commands for Music
music(client, {
	prefix: prefix,          // Prefix for the commands.
	global: false,        // Server-specific queues.
	maxQueueSize: 25,     // Maximum queue size of 25.
	clearInvoker: true,   // If permissions applicable, allow the bot to delete the messages that invoke it (start with prefix).
    helpCmd: 'mhelp',     //Sets the name for the help command.
    playCmd: 'play',     //Sets the name for the 'play' command.
    volumeCmd: 'volume',  //Sets the name for the 'volume' command.
    leaveCmd: 'begone'    //Sets the name for the 'leave' command.
});

// Bot logs in using the token variable
client.login(token);